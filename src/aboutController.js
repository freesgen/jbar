

exports.init = function(views){
$(".about-nav button").on('click',function(e){
  var documentName = $(this).text().toLowerCase().replace(" ","")
  views.getDocumentation(documentName)
})



$(".close span").on('click',function(){
  $(".about-screen").animate({opacity:0,width:0,height:0},600,function(){
    $(".about-screen").remove()
  })
  $(".editor-tools").css({visibility:"visible"})
  
})

}